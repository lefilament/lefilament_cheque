# -*- coding: utf-8 -*-

# © 2017 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import fields, models


class LefilamentCheque(models.Model):
    _name = 'lefilament.cheque'
    _order = 'name desc'

    name = fields.Char("Numéro", required=True,)
    date_issue = fields.Date("Date d'émission", required=True,)
    date_collection = fields.Date("Date d'encaissement")
    partner_id = fields.Many2one("res.partner", string="Parteraire")
    description = fields.Char("Détail", required=True,)
    amount = fields.Float("Montant", required=True,)
    status = fields.Selection([("current", "en cours"),
                               ("collected", "encaissé"),
                               ("cancel", "annulé")],
                              string="Statut",
                              default="current")
