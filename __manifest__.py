# -*- coding: utf-8 -*-

# © 2017 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    'name': 'Le Filament - Chèques',

    'summary': """Suivi chèques""",

    'version': '10.0.1.0',
    'license': 'AGPL-3',
    'description': """
        Liste et statut des chèques émis
    """,
    'author': 'LE FILAMENT',
    'category': 'Account',
    'depends': ['account'],
    'contributors': [
        'Benjamin Rivier <benjamin@le-filament.com>',
    ],
    'website': 'http://www.le-filament.com',
    'data': [
        'security/ir.model.access.csv',
        'views/lefilament_cheque_view.xml',
    ],
    'qweb': [
        'static/src/xml/*.xml',
    ],
}
